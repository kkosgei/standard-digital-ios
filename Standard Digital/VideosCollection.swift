//
//  VideosCollection.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 12/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class VideosCollection: UIViewController ,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    
    @IBOutlet weak var videosCollection: UICollectionView!
    
    var videos = [Story]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        videosCollection.delegate = self
        
        videosCollection.dataSource = self

        // Do any additional setup after loading the view.
    }


    func menuFunc(sender: UIButton) {
        
        if sender.tag == 1 {
        
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return videos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! videosCollectionCell
        
        cell.video = videos[indexPath.row]
        
        cell.closeB.layer.cornerRadius = 15
        
        cell.closeB.addTarget(self, action: #selector(VideosCollection.menuFunc), for: .touchUpInside)
        
        return cell
        
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:self.view.frame.size.width, height:self.view.frame.size.height)

        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "videosPlayer") as! VideosPlayer
        
        viewController.currentVideo = videos[indexPath.row]
        viewController.videos = self.videos
        
        self.navigationController?.pushViewController(viewController, animated: true)
    
    
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
