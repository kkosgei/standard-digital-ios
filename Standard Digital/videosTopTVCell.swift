//
//  videosTopTVCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 12/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class videosTopTVCell: UITableViewCell {

   
    @IBOutlet weak var videoTitle : UILabel!
    
    @IBOutlet weak var postedTime : UILabel!
    
    
    var video : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        
        videoTitle.text = video?.title.convertHtml().string
        postedTime.text = Utils.elapsedTime(string: (video?.date)!)
    }

    
    
}
