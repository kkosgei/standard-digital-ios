//
//  SavedArticleView.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class SavedArticleView: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate{
    
    @IBOutlet weak var bckB: UIButton!
    
    @IBOutlet weak var bckBView: UIView!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var storyTime: UILabel!
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyAuthor: UILabel!
    
    @IBOutlet weak var storyContent: UITextView!
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var parentView: UIView!
    
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    
    
    
    var relatedStory = [Story]()
    
    var story : SavedStory?
    
    var api = StandardAPI()
    
    var relatedStories = [Story]()
    
    var saved : Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        storyContent.adjustUITextViewHeight(arg: self.storyContent)
        
        storyImage.image(fromUrl: (story?.img)!)
        storyTime.text = Utils.elapsedTime(string: (story?.date)!)
        storyTitle.text = story?.title.convertHtml().string
        storyAuthor.text = story?.author.convertHtml().string
        storyContent.text = story?.story.convertHtml().string
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        bckB.addGestureRecognizer(backTap)
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        setupTextViewSize()
        
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
 
    
    
    
  
    
    
    func setupTextViewSize() {
        
        let fixedWidth = storyContent.frame.size.width
        
        storyContent.sizeThatFits(CGSize(fixedWidth,CGFloat.greatestFiniteMagnitude))
        
        let newSize = storyContent.sizeThatFits(CGSize(fixedWidth,CGFloat.greatestFiniteMagnitude))
        
        var newFrame = storyContent.frame
        
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        storyContent.frame = newFrame
        
        storyContent.isScrollEnabled = false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
