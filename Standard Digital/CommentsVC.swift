//
//  CommentsVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 18/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import WebKit

class CommentsVC: UIViewController, UIGestureRecognizerDelegate {
    
    private static var  NUMBER_OF_COMMENTS : Int = 5
    
    
    @IBOutlet weak var commentsWebView: UIWebView!
    
    @IBOutlet weak var bckB: UIButton!
    
    @IBOutlet weak var bckBView: UIView!
    
    @IBOutlet weak var commentsVCTitle: UILabel!
    
    
    var story: Story?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(self.closeFunc(sender:)))
        bckB.addGestureRecognizer(closeTap)
        bckBView.addGestureRecognizer(closeTap)
        
        loadComments()

    }

    
    func closeFunc(sender: UITapGestureRecognizer) {
        
        
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    
    func loadComments() {
        
        
        let html =  "<!doctype html> <html lang=\"en\"> <head></head> <body> " +
            "<div id=\"fb-root\"></div> <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6\"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));</script> " +
            "<div class=\"fb-comments\" data-href=\"https://www.standardmedia.co.ke\" " +
            "data-numposts=\"\(CommentsVC.NUMBER_OF_COMMENTS)\" data-order-by=\"reverse_time\">" +
        "</div> </body> </html>";
        
//        let htmlCode = "<h1>Hello Pizza!</h1>"

        commentsWebView.loadHTMLString(html, baseURL: NSURL(string: "https://www.standardmedia.co.ke")! as URL)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
