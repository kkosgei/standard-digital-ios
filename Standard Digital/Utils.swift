//
//  Utils.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 27/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift
import PKHUD
import Alamofire
import CoreData

class Connectivity {
    
    class func isConnectedToInternet() -> Bool {
        
        return NetworkReachabilityManager()!.isReachable
        
    }
    
}





public class Utils {
    
    
    public static func toggleDrawer(){
        
        if let drawerController = HomeTabBarController.context?.parent as? KYDrawerController {
            
            if(drawerController.drawerState == .closed){
                drawerController.setDrawerState(.opened, animated: true)
            } else {
                drawerController.setDrawerState(.closed, animated: true)
            }
        }
        
    }
    
    
    
    
    static func shaketextFieldAnimation(myView:UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue.init(cgPoint: CGPoint(myView.center.x - 10, myView.center.y))
        animation.toValue = NSValue.init(cgPoint: CGPoint(myView.center.x + 10,myView.center.y))
        myView.layer.add(animation, forKey: "position")
        
    }
    
    class func showAnimator() {
        
        
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        PKHUD.sharedHUD.dimsBackground = true
        PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled = true
        PKHUD.sharedHUD.show()
    }
    
    
    class func showDownloadingAnimator() {
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView(title: "Downloading", subtitle: "Please Wait...")
        PKHUD.sharedHUD.dimsBackground = true
        PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled = false
        PKHUD.sharedHUD.show()
        
    }
    
    
    class func hideAnimator() {
        
        DispatchQueue.main.async {
            PKHUD.sharedHUD.hide()
        }
        
        
    }
    
    

    
    class func clearUserLoggedIn() {
        
        let loggedInFlag = 0
        
        UserDefaults.standard.set(loggedInFlag, forKey: "User_LoggedInKey")
        UserDefaults.standard.set("", forKey: "User_UsernameKey")
        UserDefaults.standard.set("", forKey: "User_EmailKey")
        UserDefaults.standard.set("", forKey: "User_NameKey")
        UserDefaults.standard.set("", forKey: "User_ImageUrl")
        UserDefaults.standard.set("", forKey: "img")
        
        print("persisted clearUserLoggedIn")
        
    }
    
    class func saveUserLoggedIn(user: User) {
        
        let loggedInFlag = 1
        
        UserDefaults.standard.set(loggedInFlag, forKey: "User_LoggedInKey")
        UserDefaults.standard.set(user.email, forKey: "User_EmailKey")
        UserDefaults.standard.set(user.name, forKey: "User_NameKey")
        
        print("persisted saveUserLoggedIn \(user.email)")
        
    }
    
    
    class func fetchUser() -> User? {
        
        let prefs = UserDefaults.standard
        
        if let userEmail = prefs.string(forKey: "User_EmailKey") {
            
            print("Fetch User")
            
            let user = User(username: "", email: userEmail, name: "", imageUrl: "")
            
            return user
            
        }
        
        return nil
    }
    class func getDate() -> String {
        
        let date = Date()
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        let result = formatter.string(from: date)
        
        return result
    }
    
    
    class func getTimeInMilliSeconds() -> Int{
        
        return Date().millisecondsSince1970
        
    }
    

    
   
    
    class func elapsedTime (string: String) -> String
    {
        //just to create a date that is before the current time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_KE")
        let before = dateFormatter.date(from: string)!
        
        //getting the current time
        let now = Date()
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.zeroFormattingBehavior = .dropAll
        formatter.maximumUnitCount = 1 //increase it if you want more precision
        formatter.allowedUnits = [.year, .month, .weekOfMonth, .day, .hour, .minute]
        formatter.includesApproximationPhrase = false //to write "About" at the beginning
        
        
        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")
        let timeString = formatter.string(from: before, to: now)
        return String(format: formatString, timeString!)
    }
    
    public static func storeProfilePic(img:UIImage!) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "ProfilePic"))
        do {
            try managedContext.execute(DelAllReqVar)
            
            
            let entity = NSEntityDescription.entity(forEntityName: "ProfilePic", in: managedContext)!
            
            let imageData:Data = UIImageJPEGRepresentation(img, 1)!
            //UIImagePNGRepresentation(img)!
            let base64String = imageData.base64EncodedString()
            
            let loginData = NSManagedObject(entity: entity, insertInto: managedContext)
            
            loginData.setValue(base64String, forKeyPath: "img")
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            
            
        }
        catch {
            print(error)
        }
        
    }
    
    public static func retrieveImage() -> UIImage {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate!
        
        let managedContext = appDelegate?.persistentContainer.viewContext
        
        let fetchRequestUserData = NSFetchRequest<NSManagedObject>(entityName: "ProfilePic")
        
        do {
            let loginData = try (managedContext?.fetch(fetchRequestUserData))! as [NSManagedObject]
            
            
            if loginData.count > 0 {
                
                let photoinData = loginData[0].value(forKeyPath: "img") as! String
                
                let dataDecoded : Data = Data(base64Encoded: photoinData, options: .ignoreUnknownCharacters)!
                let decodedimage = UIImage(data: dataDecoded)
                return decodedimage!
                
                
            }
        } catch {
            
        }
        
        return #imageLiteral(resourceName: "gray")
        
    }

    
    
    
   
}

extension UIImageView {
    public func image(fromUrl urlString: String) {
        guard let url = URL(string: urlString) else {
            print("Couldn't create URL from \(urlString)")
            return
        }
        let theTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let response = data {
                DispatchQueue.main.async {
                    self.image = UIImage(data: response)
                }
            }
        }
        theTask.resume()
    }
}

extension String {
    
    func aesEncrypt(key: String, iv: String) throws -> String {
        let data = self.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv, blockMode: .CBC, padding: PKCS7()).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.toHexString()
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
}


extension CGRect{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat) {
        self.init(x:x,y:y,width:width,height:height)
    }
    
}
extension CGSize{
    init(_ width:CGFloat,_ height:CGFloat) {
        self.init(width:width,height:height)
    }
}
extension CGPoint{
    init(_ x:CGFloat,_ y:CGFloat) {
        self.init(x:x,y:y)
    }
}


extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
            return nil
        }
    }
    var unicodes: [UInt32] { return unicodeScalars.map{$0.value} }
}

extension String{
    func convertHtml() -> NSAttributedString{
        
        
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}

extension Date {
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
}

extension UITextView {
    
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}




