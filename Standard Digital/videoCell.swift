//
//  videoCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 11/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class videoCell: UITableViewCell {

    var topStory : Story? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var videosCount: UILabel!
    
    
    func updateCell() {
        
        
        
        let videoId = topStory?.youtube_video_id.replacingOccurrences(of: "\"", with: "")
        
        let imageUrl = "https://img.youtube.com/vi/"+videoId!+"/sddefault.jpg"
        storyImage.image(fromUrl: imageUrl)
        
        storyTitle.text = topStory?.title.convertHtml().string
        
        
    }

}
