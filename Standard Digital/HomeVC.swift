//
//  HomeVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import CoreData


class HomeVC: UIViewController , UITableViewDelegate, UITableViewDataSource , FetchNewsProtocol,FetchVideosProtocol, FetchOpinionProtocol {
    
    @IBOutlet weak var menuB: UIImageView!
    
    @IBOutlet weak var homeTitle: UILabel!
    
    @IBOutlet weak var searchBView: UIView!
    
    @IBOutlet weak var searchB: UIButton!
    
    @IBOutlet weak var uReport: UIButton!
    
    @IBOutlet weak var profileB: UIButton!
    
    @IBOutlet weak var menuBView : UIView!
    
    
    
    
    @IBOutlet weak var homeTV: UITableView!
    
    var stories = [Story]()
    
    var videos = [Story]()
    
    var opinion = [Story]()
    
    let api = StandardAPI()
    
    var story : Story?
    
    var link = ""
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Open / Close Drawer
        let menuTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        menuB.addGestureRecognizer(menuTap)
        menuBView.addGestureRecognizer(menuTap)
        
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(self.searchFunc(sender:)))
        searchBView.addGestureRecognizer(searchTap)
        searchB.addGestureRecognizer(searchTap)
        
        let profileTap = UITapGestureRecognizer(target: self, action: #selector(self.profileFunc(sender:)))
        profileB.addGestureRecognizer(profileTap)
        
        let ureportTap = UITapGestureRecognizer(target: self, action: #selector(self.ureportFunc(sender:)))
        uReport.addGestureRecognizer(ureportTap)
        
        
        
        homeTV.delegate = self
        homeTV.dataSource = self
        
        let rc = UIRefreshControl()
        rc.backgroundColor = UIColor.white
        
        rc.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        homeTV.refreshControl = rc
        
        
        api.fetchNewsDelegate = self
        api.fetchVideosDelegate = self
        api.fetchOpinionDelegate = self
        
        if Connectivity.isConnectedToInternet() {
            
            api.fetchNews(catID: SDConstant.CATEGORY_ID_TOP_STORIES, lastID: SDConstant.LAST_ID)
            api.fetchVideos(catID: SDConstant.CAEGORY_ID_VIDEOS, lastID: SDConstant.LAST_ID)
            api.fetchOpinionNews(catID: SDConstant.CATEGORY_ID_OPINION, lastID: SDConstant.LAST_ID)
            
        }else {
            
            
            let controller = UIAlertController(title: "No Internet Detected", message: "This app requires an Internet connection", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            controller.addAction(ok)
            controller.addAction(cancel)
            
            present(controller, animated: true, completion: nil)
            
        }
        
       
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        api.fetchNews(catID: SDConstant.CATEGORY_ID_TOP_STORIES, lastID: SDConstant.LAST_ID)
        api.fetchVideos(catID: SDConstant.CAEGORY_ID_VIDEOS, lastID: SDConstant.LAST_ID)
        api.fetchOpinionNews(catID: SDConstant.CATEGORY_ID_OPINION, lastID: SDConstant.LAST_ID)
        refreshControl.endRefreshing()
    }

 
    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        Utils.toggleDrawer()
        
    }
    
    func searchFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "search") as! SearchVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func profileFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func ureportFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "uReport") as! UReportVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    
    func sharePressed(sender : UIButton) {
        
        let activityVC = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
        
        
    }
    
    
    
    func didReceiveSuccess(news: [Story]) {
        
        
        Utils.hideAnimator()
        
        stories = news
        
        homeTV.reloadData()
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        
    }
    
    
    func didFecthVideosSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        videos = news
        
        homeTV.reloadData()
        
    }
    
    func didFecthOpinionSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        opinion = news
        
        homeTV.reloadData()
        
    }
    
    

    
    
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
       
        return stories.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "topStory", for: indexPath)   as! topStoryCell
            
            if self.stories.count < 1 {
                
                return cell
                
            }else {
            
             cell.topStory = stories[indexPath.row]

             link = (cell.topStory?.link)!
            
            
            cell.shareStory.addTarget(self, action: #selector(self.sharePressed), for: .touchUpInside)
            
            return cell
            }
        }else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "video", for: indexPath)   as! videoCell

            if self.videos.count < 1 {
                
                return cell
            }else {
            
            
            print(":::::::::::::::\(videos)")
            cell.topStory = self.videos[indexPath.row]

            return cell
            }
            
        }else if indexPath.row == 10 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "opinion", for: indexPath)   as! opinionCell
            
            cell.topStory = self.opinion[indexPath.row]
            

            return cell
            
        }else  {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsThree", for: indexPath)   as! newsThreeCell
            
            cell.topStory = stories[indexPath.row]
            
            link = stories[indexPath.row].link
            
            cell.shareStory.addTarget(self, action: #selector(self.sharePressed), for: .touchUpInside)

            
            return cell
            
        }
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 210
        }else if indexPath.row == 10 {
            
            return 160
            
        }else if indexPath.row == 5 {
            
            return 160
            
        }else  {
            
            return 150
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        if indexPath.row == 5 {
            
          let viewController =   storyboard.instantiateViewController(withIdentifier: "videosCollection") as! VideosCollection
          viewController.videos = self.videos
          self.navigationController?.pushViewController(viewController, animated: true)
            
            
        }else if indexPath.row == 10 {
            

            let viewController = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            viewController.catID = SDConstant.CATEGORY_ID_OPINION
            viewController.catTitle = "OPINION"
            self.navigationController?.pushViewController(viewController, animated: true)
            
            
        }else {
            
            
            let viewController =   storyboard.instantiateViewController(withIdentifier: "fullStory") as! FullStoryVC
            viewController.story = stories[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)

            
        }
        
        
    }

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
