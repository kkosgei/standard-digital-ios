//
//  Settings.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import CoreData

class Settings: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    
    
    @IBOutlet weak var settingsTV : UITableView!
    
    @IBOutlet weak var settingsTitle : UILabel!
    
    @IBOutlet weak var bckB : UIButton!
    
    @IBOutlet weak var bckBView : UIView!
    
    
    var settings = ["Rate Us","Terms Of Service","Privacy Policy", "Spread the Love","Sign Out"]

    override func viewDidLoad() {
        super.viewDidLoad()

       settingsTV.delegate = self
    
       settingsTV.dataSource = self
        
       settingsTV.reloadData()
        
       settingsTitle.text = "SETTINGS"

        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(self.closeFunc(sender:)))
        bckB.addGestureRecognizer(backTap)
        bckBView.addGestureRecognizer(backTap)
        
    }

    func closeFunc(sender: UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
  
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return settings.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! settingsCell

        cell.circleView.layer.cornerRadius = 25
        cell.circleView.layer.borderWidth = 2
        cell.circleView.layer.borderColor = UIColor(hexString: "#0F1040").cgColor
        
        cell.settingsTitle.text =  settings[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("Implement on click item")
        
        switch indexPath.row {
            
        case 0:
            
            let appId = "908120328"
            let url_string = "itms-apps://itunes.apple.com/app/id\(appId)"
            if let url = URL(string: url_string) {
                UIApplication.shared.open(url)
            }
        case 1:
            
            
            let termsUrl = URL(string: "https://www.standardmedia.co.ke/?contentPage=terms-of-service")!
            
            UIApplication.shared.open(termsUrl, options: [ : ], completionHandler: nil)
            
        case 2:
            
            let privacyUrl = URL(string: "https://www.standardmedia.co.ke/?contentPage=terms-of-service")!
            
            UIApplication.shared.open(privacyUrl, options: [ : ], completionHandler: nil)
            
        case 3:
            
            
            let activityVC = UIActivityViewController(activityItems: ["https://itunes.apple.com/us/app/standard-digital/id908120328?ls=1&mt=8"], applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = self.view
            
            self.present(activityVC, animated: true, completion: nil)
            
        case 4:
            
            UserDefaults.standard.set("", forKey: "User_EmailKey")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.persistentContainer.viewContext
            let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "ProfilePic"))
            
            do {
                
                try managedContext.execute(DelAllReqVar)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"login")
                let navController = UINavigationController.init(rootViewController: viewController)
                
                if let window = self.view.window, let rootViewController = window.rootViewController {
                    var currentController = rootViewController
                    while let presentedController = currentController.presentedViewController {
                        currentController = presentedController
                    }
                    currentController.present(navController, animated: true, completion: nil)

                
            }
            
            
            
            }catch {
                
                print(error.localizedDescription)
            }
         
            
        default:
            
            let appStoreUrl = URL(string: "https://itunes.apple.com/us/app/standard-digital/id908120328?ls=1&mt=8")!
            
            UIApplication.shared.open(appStoreUrl, options: [ : ], completionHandler: nil)
            
        }
        
    }




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
