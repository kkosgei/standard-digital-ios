//
//  StoryEntity+CoreDataProperties.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 19/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import CoreData


extension StoryEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoryEntity> {
        return NSFetchRequest<StoryEntity>(entityName: "SavedEntity")
    }

    @NSManaged public var author: String?
    @NSManaged public var category_id: Int16
    @NSManaged public var date: String?
    @NSManaged public var id: Int16
    @NSManaged public var img: String?
    @NSManaged public var keywords: String?
    @NSManaged public var link: String?
    @NSManaged public var story: String?
    @NSManaged public var title: String?
    @NSManaged public var youtube_video_id: String?

}
