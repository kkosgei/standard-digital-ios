//
//  SavedStory.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation


public class SavedStory{
    
    public var id:Int
    public var category_id:Int
    public var title:String
    public var author:String
    public var story:String
    public var img:String
    public var link:String
    public var date:String
    public var keywords:String
    public var youtube_video_id:String
    
    init( id:Int, category_id:Int, title:String, author:String,
         story:String, img:String, link:String, date:String,
         youtube_video_id:String, keywords: String){
        self.id = id
        self.category_id = category_id
        self.title = title
        self.author = author
        self.story = story
        self.img = img
        self.link = link
        self.date = date
        self.youtube_video_id = youtube_video_id
        self.keywords = keywords
    }
    
}
