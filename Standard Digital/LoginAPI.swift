//
//  LoginAPI.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 17/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol LoginProtocol {
    
    func didReceiveSuccess( username:String, email:String, name:String, imageUrl : String)
    
    func didReceiveError(error:String)
        
}

protocol feedbackProtocol {
    
    func didReceiveSuccess (message: String)
    
    func didReceiveError(error: String)
    
}

class LoginAPI  {
    
    //    let feedbackURL = "https://www.standardmedia.co.ke/rss/mobilev2/feedback_req.php"
    let feedbackURL = "https://www.standardmedia.co.ke/rss/mobilev2/feedback_req_v2.php"
    
    var delegate : LoginProtocol!
    
    var feedbackProtocol : feedbackProtocol!
    
    init(myDelegate:LoginProtocol){
        delegate = myDelegate
    }
    
    init() {
        
    }
    // API Methods
    
    func login(username: String, password: String) {
        
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs","name":"\(username)","email":"\(username)","password":"\(password)"]
        
        Alamofire.request(SDConstant.signInUrlV2, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {  response in
            
            print("Logging In>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            
            let json = JSON(response.result.value as Any)
            
            print("\(json)")
            
            if  let myresponse = json["status"].int {
                
                if myresponse == 1 {
                    
                    let user = json["user"].dictionary
                    
                    
                    
                    let username = user?["username"]?.stringValue
                    let email = user?["email"]?.stringValue
                    let imageUrl = user?["img"]?.stringValue
                    let name = user?["name"]?.stringValue
                    
                    
                    
                    self.delegate.didReceiveSuccess(username: username!, email: email!, name: name!, imageUrl: imageUrl!)
                    
                    print("This -------------->\(String(describing: email))")
                    
                }
                else {
                    let message = json["message"].stringValue
                    
                    self.delegate.didReceiveError(error: message)
                }
            }
            
        }
        
    }
    
    func signup(username: String, email:String,password: String) {
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "email":"\(email)","name":"\(username)","password":"\(password)"]
        
        Alamofire.request(SDConstant.registerUrlV2, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            print("Signing Up >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            
            let json = JSON(response.result.value!)
            
            print("\(json)")
            
            if let myResponse = json["status"].int {
                
                if myResponse == 1 {
                    
                    
                    let user = json["user"].dictionary
                    
                    let username = user?["username"]?.stringValue
                    let email = user?["email"]?.stringValue
                    let name = user?["names"]?.stringValue
                    let image = ""
                    
                    self.delegate.didReceiveSuccess(username: username!, email: email!, name: name!, imageUrl: image)
                    
                    
                }else if myResponse != 1 {
                    
                    let message = json["message"].stringValue
                    self.delegate.didReceiveError(error: message)
                    
                }
                else {
                    
                    self.delegate.didReceiveError(error: "")
                }
                
            }
            
        }
        
    }
    
    func sendFeedback(messsage : String) {
        
        let username = UserDefaults.standard.string(forKey: "User_EmailKey")
        
        //        let encryptedUsername = try! username!.aesEncrypt(key: key, iv: iv)
        
        let messageString = messsage
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "email":"\(username!)","message":"\(messageString)"]
        
        
        Alamofire.request(feedbackURL, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {  response in
            
            print("Sending  Feedback>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            
            let json = JSON(response.result.value as Any)
            
            print("\(json)")
            
            if  let myresponse = json["status"].int {
                
                if myresponse == 1 {
                    
                    let message = json["message"].stringValue
                    
                    self.feedbackProtocol.didReceiveSuccess(message: message)
                    
                    print("This -------------->\(String(describing: message))")
                    
                }
                else if myresponse != 1 {
                    let message = json["message"].stringValue
                    
                    self.feedbackProtocol.didReceiveError(error: message)
                }
                    
                else {
                    
                    self.feedbackProtocol.didReceiveError(error: " ")
                    
                }
            }
            
        }
        
        
        
        
    }
    
}

