//
//  SdAPI.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 29/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation

//
//  StandardAPI.swift
//  Epaper
//
//  Created by Kelvin Kosgei on 16/08/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CryptoSwift
import PKHUD
import CoreData


protocol StandardAPIProtocol {
    
    func didReceiveAPIResult(image: String,date: String,title: String)
    func didRecieveErrorDuringProcessing(error:String)
    //    func didRecieveMagazineItems(items:[NewspaperItem])
    //    func refreshRecievedData(newsItems:[NewspaperItem])
    //    func didFinishDowloading()
    //    func downloadProgress(progress:Double)
    
}

protocol MakeOrderProtocol {
    
    func didReceiveSuccess(message: String)
    
}
protocol FetchPreviousNewsProtocol {
    
    func didReceiveSuccesss(items: [Story])
    
}

protocol FetchUserSubscriptionsProtocol {
    
    func didReceiveSuccess(subscriptions: [Story])
    
    func didReceiveError(error: String)
    
}

protocol FetchNewsProtocol {
    
    func didReceiveSuccess(news: [Story])
    
    func didReceiveError(error: String)
    
}

protocol FetchOpinionProtocol {
    
    func didFecthOpinionSuccess(news: [Story])
    
    func didReceiveError(error: String)
    
}

protocol FetchVideosProtocol {
    
    func didFecthVideosSuccess(news: [Story])
    
    func didReceiveError(error: String)
    
}

protocol CheckPaperSubscribedProtocol {
    
    func didReceiveCheckPaperStatus(status: Int,message: String,story: Story)
    
}

protocol DownloadPaperProtocol {
    
    func didFinishDownloading(pdfPath: String)
    
}

protocol fetchKtnLiveUrl {
    
    func didRetrieveVideoID(video : LiveKtn)
    
}

class StandardAPI {
    
    var newspaperCoverPage = ""
    var newspaperPublishDate = ""
    var pdfLink = " "
    
    var paperSize = 0
    var paperType = 0
    var paperTitle = " "
    
    var nairobianCoverPage = ""
    var nairobianPublishDate = ""
    var nairobianPdfLink = " "
    
    var nairobianSize = 0
    var nairobianType = 0
    var nairobianTitle = " "
    
    
    //    let fetchPapersUrl = "https://www.standardmedia.co.ke/rss/mobilev2/fetch_papers.php"
    
    let fetchPapersUrl = "https://www.standardmedia.co.ke/rss/mobilev2/fetch_papers_v2.php"
    
    //    let checkPaperSubscribedUrl = "https://www.standardmedia.co.ke/rss/mobilev2/check_paper_subscribed.php"
    
    let checkPaperSubscribedUrl = "https://www.standardmedia.co.ke/rss/mobilev2/check_paper_subscribed_v2.php"
    
    //    let userSubscriptionsUrl = "https://www.standardmedia.co.ke/rss/mobilev2/user_subscriptions.php"
    
    let userSubscriptionsUrl = "https://www.standardmedia.co.ke/rss/mobilev2/user_subscriptions_v2.php"
    
    //    let makeOrderUrl = "https://www.standardmedia.co.ke/rss/mobilev2/make_order.php"
    
    let makeOrderUrl = "https://www.standardmedia.co.ke/rss/mobilev2/make_order_v2.php"
    
    
    
    
    var delegate: StandardAPIProtocol!
    var makeOrderDelegate: MakeOrderProtocol!
    var fetchPreviousDelegate: FetchPreviousNewsProtocol!
    var fetchSubscriptionsDelegate: FetchUserSubscriptionsProtocol!
    var fetchNewsDelegate: FetchNewsProtocol!
    var checkPaperSubscribeddelegate : CheckPaperSubscribedProtocol!
    var downloadPaperDelegate : DownloadPaperProtocol!
    var getLiveVideoDelegate : fetchKtnLiveUrl!
    var fetchOpinionDelegate : FetchOpinionProtocol!
    var fetchVideosDelegate : FetchVideosProtocol!
    
    var newsItem = [Story]()
    var ktnVideo : LiveKtn?
    
    
    init () {
        
        
    }
    
    
    
    func fetchNews(catID: Int,lastID: Int) {
    
        
        
        Utils.showAnimator()
        
        
        
        print("Fetching News ---------------------->>>>>>>>>>>>")
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "cat_id":"\(catID)","last_id":"\(lastID)","product":"1"]
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is fetch news Response ------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The New Fetched papers\(news.title)")
                    
                    
                }
                
                self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
            }
            
            
        }
        
    }
    
//    Fetch Opinion News
    
    func fetchOpinionNews(catID: Int,lastID: Int) {
        
        Utils.showAnimator()
        
        
        
        print("Fetching News ---------------------->>>>>>>>>>>>")
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "cat_id":"\(catID)","last_id":"\(lastID)","product":"1"]
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is fetch news Response ------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The New Fetched papers\(news.title)")
                    
                    
                }
                
                self.fetchOpinionDelegate.didFecthOpinionSuccess(news: items)
                
            }
            
            
        }
        
    }
    
//    Fetch Videos
    
    func fetchVideos(catID: Int,lastID: Int) {
        
        Utils.showAnimator()
        
        
        print("Fetching News ---------------------->>>>>>>>>>>>")
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "cat_id":"\(catID)","last_id":"\(lastID)","product":"1"]
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is fetch news Response ------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The New Fetched papers\(news.title)")
                    
                    
                }
                
                self.fetchVideosDelegate.didFecthVideosSuccess(news: items)
                
            }
            
            
        }
        
    }
    
    
    
//    Fetch Photos
    
    
    func fetchPhotos(catID: Int, lastID: Int) {
        
        
        Utils.showAnimator()
        
        
        print("Fetching Photos ------------------------->>>>>>>>>>>>")
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "cat_id":"\(catID)","last_id":"\(lastID)","product":"1"]
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            Utils.hideAnimator()
            
            if let err = response.error {
                
                self.delegate.didRecieveErrorDuringProcessing(error: err.localizedDescription)
                
            }
            
            let json = JSON(response.result.value!)
            
            print("Fetch Photos Response ---------------->>>>>>>>>>>>\(json)")
            
            
            if let fetchedPhoto = json["data"].array {
                
                var items = [Story]()
                
                
                for fetchedPhoto in fetchedPhoto  {
                    
                    
                    let fetchedStory = Story(JSONData: fetchedPhoto.dictionary!)
                    
                    items.append(fetchedStory)
                    
                }
                
                
                let _ = self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
                
                for item in items {
                    
                    print("\(item.title)")
                    
                }
                
                print(items.count)
                
                print(items)
                
                
                
            }
            
            
        }
        
        
    }
    
    
    // Fetch Gallery Photos
    
    
    func fetchGalleryPhotos(catID: Int, lastID: Int, albumID :Int) {
        
        
        Utils.showAnimator()
        
        
        print("Fetching Gallery  ------------------------->>>>>>>>>>>>")
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs", "cat_id":"\(catID)","last_id":"\(lastID)","product":"1","albumid":"\(albumID)"]
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            Utils.hideAnimator()
            
            if let err = response.error {
                
                self.delegate.didRecieveErrorDuringProcessing(error: err.localizedDescription)
                
            }
            
            let json = JSON(response.result.value!)
            
            print("Fetch Gallery Response ---------------->>>>>>>>>>>>\(json)")
            
            
            if let fetchedPhoto = json["data"].array {
                
                var items = [Story]()
                
                
                for fetchedPhoto in fetchedPhoto  {
                    
                    
                    let fetchedStory = Story(JSONData: fetchedPhoto.dictionary!)
                    
                    items.append(fetchedStory)
                    
                }
                
                
                let _ = self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
                
                for item in items {
                    
                    print("\(item.title)")
                    
                }
                
                print(items.count)
                
                print(items)
                
                
                
            }
            
            
        }
        
        
    }
    
    //Fetch Related Stories
    
    func fetchRelated(catID: Int, lastID: Int, keywords: String) {
        
        Utils.showAnimator()
        
        
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs","cat_id":"\(catID)","last_id":"\(lastID)","product":"1", "keywords":"\(keywords)"]
        
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is fetch Related News Response ---------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The fetched related News\(news.title)")
                    
                    
                }
                
                self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
            }
            
            
        }
        
    }
    
    
//    FetchSearch
    
    
    func fetchSearch(catID: Int, lastID: Int, searchCritea: String) {
        
        Utils.showAnimator()
        
        
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs","cat_id":"\(catID)","last_id":"\(lastID)","product":"1", "searchcriteria":"\(searchCritea)"]
        
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is  search News  Response ---------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The search News\(news.title)")
                    
                    
                }
                
                self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
            }
            
            
        }
        
    }
    
    
//    Upload Reportv-- To finish.
    
    
    func uploadReport(catID: Int, lastID: Int, name: String,title: String, story: String) {
        
        Utils.showAnimator()
        
        let email = UserDefaults.standard.string(forKey: "User_EmailKey")?.trim()

        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs","email":"\(String(describing: email))","name":"\(name)","catid":"\(catID)","title":"\(title)","story":"\(story)"]
        
        
        Alamofire.request(SDConstant.fetchNewsUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is fetch Papers Response ---------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["data"].array {
                
                var items = [Story]()
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = Story(JSONData: newsItem.dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The New Fetched papers\(news.title)")
                    
                    
                }
                
                self.fetchNewsDelegate.didReceiveSuccess(news: items)
                
            }
            
            
        }
        
    }
    
    //    Make Order
    
    
    
    func makeOrder(subId: String) {
        
        Utils.showAnimator()
        
        let email = UserDefaults.standard.string(forKey: "User_EmailKey")?.trim()
        
        
        //        let encryptedEmail: String = try! email!.aesEncrypt(key: key, iv: iv)
        
        let subId = subId
        
        //        let encryptedSubId : String = try! subId.aesEncrypt(key: key, iv: iv)
        let date = Utils.getDate()
        
        //        let date = try! Utils.getDate().aesEncrypt(key: key, iv: iv)
        
        print("This is encrypted Date \(date)")
        
        print("Processing Order Steps For \(String(describing: email)).")
        
        
        
        let authentication = ["userpass":"ob@@@gj!!!hdfs34!5xfsghs","email":"\(email!)","subid":"\(subId)","date":"\(date)"]
        
        
        Alamofire.request(makeOrderUrl, method: .post, parameters: authentication, encoding: URLEncoding.default, headers: nil).responseJSON {
            
            response in
            
            
            if let err = response.error {
                
                //                self.delegate.didRecieveErrorDuringProcessing(error: err.localizedDescription)
                print("Error \(err.localizedDescription)")
                
            }
            
            let json = JSON(response.result.value!)
            
            print("This is how to make an order --------->>>>>>> \(json)")
            
            let message = json["message"].stringValue
            
            print(message)
            
            self.makeOrderDelegate.didReceiveSuccess(message: message)
            
            
        }
        
    }
    
//    Fetch KTN Live Video ID
    
    func getLiveVideo() {
        
        Utils.showAnimator()
        
        
        
        print("Getting YouTube Video -->>>>>>>>>>>>")
        
        
        Alamofire.request(SDConstant.ktnLiveUrl).responseJSON {
            
            response in
            
            if let err = response.error {
                
                self.fetchNewsDelegate.didReceiveError(error: err.localizedDescription)
            }
            
            let json = JSON(response.result.value!)
            
            print("This is Live Video ID :::: ---------------->>>>>>>>>>>>\(json)")
            
            if let myNewsItem = json["items"].array {
                
                var items = [LiveKtn]()
                
                
                
                for newsItem in myNewsItem  {
                    
                    
                    let newpaperItem = LiveKtn(JSONData: newsItem["id"].dictionary!)
                    
                    items.append(newpaperItem)
                    
                }
                
                for news in items {
                    
                    print("The New Fetched papers :::  \(news.youtube_video_id)")
                    
                    self.ktnVideo = news
                    
                }
                
                self.getLiveVideoDelegate.didRetrieveVideoID(video: self.ktnVideo!)
                
            }
            
            
        }
        
    }
    
    
    
    
    func downloadPdf(pfdLink: String) {
        Utils.showDownloadingAnimator()
        
        let uid : String = UserDefaults.standard.string(forKey: "User_EmailKey")!
        
        
        
        //        let o_fl: String = pfdLink
        
        print("PDFLINK: \(pfdLink)")
        
        let time = Utils.getTimeInMilliSeconds()
        
        let hshToMd5 = "#epaperkey#2015#"+"\(time)"
        
        print(hshToMd5)
        
        //        let hsh = md5(hshToMd5)
        
        
        
        print(time)
        
        let baseUrl = "https://www.standardmedia.co.ke/fpdi/epaper_service.php?uid=\(uid)&o_fl=\(pfdLink)&hsh=\(hshToMd5)&tm=\(time)"
        
        
        print("Here is the base url ----------------------------------->>>>>>>>\(baseUrl)")
        
        
        
        
        let destination : DownloadRequest.DownloadFileDestination = { _,_ in
            
            let documentsURL: NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            
            print("************\(documentsURL)")
            
            let fileURL = documentsURL.appendingPathComponent("\(pfdLink).pdf")
            
            print("*****######****fileURL:",fileURL ?? "")
            
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
            
        }
        
        Alamofire.download(baseUrl, to: destination).downloadProgress(closure: { (progress) in
            print(progress.fractionCompleted)
            Utils.showDownloadingAnimator()
        }).response { response in
            
            
            
            if response.error != nil {
                
                let error = response.error
                
                print(error?.localizedDescription ?? "Error")
                
            }else{
                
                let filePath = response.destinationURL?.path
                
                self.downloadPaperDelegate.didFinishDownloading(pdfPath: filePath!)
                
                Utils.hideAnimator()
                
                print("mmmmmm",filePath ?? "")
                
                
                
                
            }
            
        }
        
        
        
        
    }
    
    
    
}
