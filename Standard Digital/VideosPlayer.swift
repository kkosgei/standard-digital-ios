//
//  VideosPlayer.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 12/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideosPlayer: UIViewController, UITableViewDelegate, UITableViewDataSource, YTPlayerViewDelegate, UIGestureRecognizerDelegate{
    
    var videos = [Story]()
    
    var currentVideo : Story?
    
    @IBOutlet weak var playerView : YTPlayerView!
    
    @IBOutlet weak var videosTV : UITableView!
    
    @IBOutlet weak var closeB : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        videosTV.delegate = self
        
        videosTV.dataSource = self
        
        playerView.delegate = self
        
        playerView.load(withVideoId: (currentVideo?.youtube_video_id)!, playerVars: SDConstant.playerVars)
        
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        closeB.addGestureRecognizer(closeTap)
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        videosTV.reloadData()
        
    }
    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.playerView.pauseVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.playerView.playVideo()
    }
    
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
    
        self.playerView.playVideo()
        
    }
    

    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return videos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "topVideo", for: indexPath) as! videosTopTVCell
        
        cell.video = currentVideo
        
        return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! videosTVCell
            
            cell.video = videos[indexPath.row]
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.playerView.load(withVideoId: videos[indexPath.row].youtube_video_id, playerVars: SDConstant.playerVars)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 100
        }else {
            
            
            return 150
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
