//
//  Drawer.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class Drawer: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    public static var accImg:UIImageView? = nil

    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var categoriesTV: UITableView!
    
    @IBOutlet weak var settingsB: UIButton!
    
    
    
    
    var categoryList = ["HOME", "BUSINESS", "POLITICS", "WORLD","LIFESTYLE", "COUNTIES", "SPORTS", "ENTERTAINMENT","EDUCATION", "COLUMNS"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Drawer.accImg = profileImage

        categoriesTV.delegate = self
        categoriesTV.dataSource = self
        // Do any additional setup after loading the view.
        
        
        setupViews()
        
        categoriesTV.reloadData()
        
        let settingsTap = UITapGestureRecognizer(target: self, action: #selector(self.settingsFunc(sender:)))
        
        settingsB.addGestureRecognizer(settingsTap)
        
        Drawer.changeImg()
        
    }

    
    
    
    func setupViews() {
        
        profileImage.layer.cornerRadius = 50
        
    }
    
    
    func settingsFunc(sender: UITapGestureRecognizer) {
        
        Utils.toggleDrawer()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "settings") as! Settings
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categoryList.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = categoriesTV.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DrawerTVC
        
        cell.categoryName.text = categoryList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch indexPath.row {
        case 0:
            Utils.toggleDrawer()

            print("Selected Home")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "drawerController") as! KYDrawerController
            let navController = UINavigationController.init(rootViewController: viewController)
            
            if let window = self.view.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(navController, animated: true, completion: nil)
                
            }
            
        case 1:
            print("Selected BUSINESS")
            
            Utils.toggleDrawer()

            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catTitle = "BUSINESS"
            newsDetailVC.catID = SDConstant.CATEGORY_ID_BUSINESS
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
            
        case 2:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_POLITICS
            newsDetailVC.catTitle = "POLITICS"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
            
        case 3:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_WORLD
            newsDetailVC.catTitle = "WORLD"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
            
        case 4:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_LIFESTYLE
            newsDetailVC.catTitle = "LIFESTYLE"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)

            
        case 5:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_COUNTIES
            newsDetailVC.catTitle = "COUNTIES"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
            
        case 6:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_SPORTS
            newsDetailVC.catTitle = "SPORTS"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)

            
        case 7:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_ENTERTAINMENT
            newsDetailVC.catTitle = "ENTERTAINMENT"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)

            
        case 8:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_EDUCATION
            newsDetailVC.catTitle = "EDUCATION"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)

            
        case 9:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newsDetailVC = storyboard.instantiateViewController(withIdentifier: "newsCategory") as! NewsCategory
            newsDetailVC.catID = SDConstant.CATEGORY_ID_COLUMNS
            newsDetailVC.catTitle = "COLUMNS"
            self.navigationController?.pushViewController(newsDetailVC, animated: true)





            
        default:
            Utils.toggleDrawer()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "drawerController") as! KYDrawerController
            let navController = UINavigationController.init(rootViewController: viewController)
            
            if let window = self.view.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(navController, animated: true, completion: nil)
                
            }

        }

        
    }
    
    
    
    public static func changeImg() {
        
        if  Utils.retrieveImage() as? UIImage != #imageLiteral(resourceName: "gray") {
            Drawer.accImg?.layer.cornerRadius = (Drawer.accImg?.frame.height)!/2
            Drawer.accImg?.clipsToBounds = true
            Drawer.accImg?.image = Utils.retrieveImage() as? UIImage
            Drawer.accImg?.contentMode = .scaleAspectFill
            Drawer.accImg?.layer.borderColor = UIColor.white.cgColor
            Drawer.accImg?.layer.borderWidth = 1
            
        }
        
    }

}
