//
//  videosCollectionCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 12/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class videosCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    
    @IBOutlet weak var photoTitle: UILabel!
    
    @IBOutlet weak var closeB : UIButton!
    
    @IBOutlet weak var playB : UIButton!
    
    @IBOutlet weak var currentPage : UILabel!
    
    @IBOutlet weak var totalVideos : UILabel!
    
    var video : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        
        let videoId = video?.youtube_video_id.replacingOccurrences(of: "\"", with: "")
        
        let imageUrl = "https://img.youtube.com/vi/"+videoId!+"/sddefault.jpg"
        
        photoImage.image(fromUrl: imageUrl)

        photoTitle.text = video?.title.convertHtml().string
        
    }
    
   
    
}
