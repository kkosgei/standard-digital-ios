//
//  topicsCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 18/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class topicsCell: UICollectionViewCell {
    
    
    @IBOutlet weak var relatedB : UIButton!
    
    
    var topic : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        relatedB.layer.cornerRadius = 19
        
        relatedB.titleLabel?.text = topic?.title.convertHtml().string
        
    }

    
}
