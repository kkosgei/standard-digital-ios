//
//  PicturesVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class PicturesVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , FetchNewsProtocol{
    
    @IBOutlet weak var menuB: UIButton!
    
    @IBOutlet weak var uReport: UIButton!
    
    @IBOutlet weak var searchB: UIButton!
    
    @IBOutlet weak var profileB: UIButton!
    
    @IBOutlet weak var photosScrollview: UICollectionView!
    
    
    var api = StandardAPI()
    
    var photos = [Story]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let menuTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        menuB.addGestureRecognizer(menuTap)
        
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(self.searchFunc(sender:)))
        searchB.addGestureRecognizer(searchTap)
        
        let profileTap = UITapGestureRecognizer(target: self, action: #selector(self.profileFunc(sender:)))
        profileB.addGestureRecognizer(profileTap)
        
        let ureportTap = UITapGestureRecognizer(target: self, action: #selector(self.ureportFunc(sender:)))
        uReport.addGestureRecognizer(ureportTap)
        
        
        
        photosScrollview.delegate = self
        
        photosScrollview.dataSource = self
        
        api.fetchNewsDelegate = self
        
        if Connectivity.isConnectedToInternet() {
            
            api.fetchPhotos(catID: SDConstant.CATEGORY_ID_PHOTOS, lastID: SDConstant.LAST_ID)

        }else {
            
            let controller = UIAlertController(title: "No Internet Detected", message: "This app requires an Internet connection", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            controller.addAction(ok)
            controller.addAction(cancel)
            
            present(controller, animated: true, completion: nil)

            
        }
        
        let rc = UIRefreshControl()
        
        rc.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: UIControlEvents.valueChanged)

        photosScrollview.refreshControl = rc
        
        
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        
        api.fetchPhotos(catID: SDConstant.CATEGORY_ID_PHOTOS, lastID: SDConstant.LAST_ID)

        
        refreshControl.endRefreshing()
    }

    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        Utils.toggleDrawer()
        
    }
    
    func searchFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "search") as! SearchVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    
    func profileFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func ureportFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "uReport") as! UReportVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func didReceiveError(error: String) {
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        photos = news
        
        photosScrollview.reloadData()
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topPhoto", for: indexPath) as! topPhotoCell
            
            cell.photo = photos[indexPath.row]
            
            return cell
            
        }else {
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainPhoto", for: indexPath) as! photoCell
            
            cell.photo = photos[indexPath.row]
            
            return cell
        }
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 {
            
            return CGSize(width:self.view.frame.size.width, height:250)

        }
        
        return CGSize(width:160, height:160)

    }
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0 {
            return 5
            
        }else {
        
        return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let viewController = storyBoard.instantiateViewController(withIdentifier: "galleryPhotos") as! galleryPhotos
        
        viewController.albumId = photos[indexPath.row].id
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
