//
//  KtnVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class KtnVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FetchNewsProtocol , fetchKtnLiveUrl, YTPlayerViewDelegate{
    
    
    @IBOutlet weak var menuB: UIButton!
    
    @IBOutlet weak var menuBView: UIView!

    @IBOutlet weak var standardTitle: UILabel!
    
    @IBOutlet weak var searchB: UIButton!
    
    @IBOutlet weak var searchBView: UIView!
    
    @IBOutlet weak var profileBView: UIView!
    
    @IBOutlet weak var profile: UIButton!
    
    @IBOutlet weak var uReport: UIButton!
    @IBOutlet weak var uReportBView: UIView!
    
    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    @IBOutlet weak var playerView: YTPlayerView!
    
    
    var videos = [Story]()
    
    var liveVideo : LiveKtn?
    
    var api = StandardAPI()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let menuTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        menuB.addGestureRecognizer(menuTap)
        menuBView.addGestureRecognizer(menuTap)
        
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(self.searchFunc(sender:)))
        searchB.addGestureRecognizer(searchTap)
        searchBView.addGestureRecognizer(searchTap)
        
        let profileTap = UITapGestureRecognizer(target: self, action: #selector(self.profileFunc(sender:)))
        profile.addGestureRecognizer(profileTap)
        
        let ureportTap = UITapGestureRecognizer(target: self, action: #selector(self.ureportFunc(sender:)))
        uReport.addGestureRecognizer(ureportTap)
        
        
        
        videosCollectionView.delegate = self
        videosCollectionView.dataSource = self
        
        api.fetchNewsDelegate = self
        api.getLiveVideoDelegate = self
        
        self.playerView.delegate = self
        
        if Connectivity.isConnectedToInternet() {
            
            api.fetchNews(catID: SDConstant.CAEGORY_ID_VIDEOS, lastID: SDConstant.LAST_ID)

            api.getLiveVideo()

        }else {
            
            let controller = UIAlertController(title: "No Internet Detected", message: "This app requires an Internet connection", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            controller.addAction(ok)
            controller.addAction(cancel)
            
            present(controller, animated: true, completion: nil)
            
            
        }
        
        
      
    }
    
    
    func searchFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "search") as! SearchVC
        self.navigationController?.pushViewController(viewController, animated: true)

        
    }
    
    func profileFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func ureportFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "uReport") as! UReportVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }

  
    override func viewWillDisappear(_ animated: Bool) {
        self.playerView.pauseVideo()
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.playerView.pauseVideo()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.playerView.playerState()
    }
    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        Utils.toggleDrawer()
        
    }
    
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        
    }
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        videos = news
        
        videosCollectionView.reloadData()
        
    }
    func didRetrieveVideoID(video: LiveKtn) {
        
        Utils.hideAnimator()
        
        liveVideo = video
        
        self.playerView.load(withVideoId: (liveVideo?.youtube_video_id)!,playerVars: SDConstant.playerVars)
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        
        self.playerView.playVideo()
        
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return videos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:155, height:120)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("To play selected Video")
        
        self.playerView.load(withVideoId: videos[indexPath.row].youtube_video_id, playerVars: SDConstant.playerVars)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videos", for: indexPath) as! videosCell
        
        cell.video = videos[indexPath.row]
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
