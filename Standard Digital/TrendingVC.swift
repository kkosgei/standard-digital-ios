//
//  TrendingVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 20/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class TrendingVC: UIViewController ,UITableViewDelegate, UITableViewDataSource ,FetchNewsProtocol{
    
    
    @IBOutlet weak var menuB: UIButton!
    
    @IBOutlet weak var standardTitle: UILabel!
    @IBOutlet weak var searchView: UIButton!
    
    @IBOutlet weak var uReport: UIButton!

    @IBOutlet weak var profile: UIButton!
    
    @IBOutlet weak var trendingTV: UITableView!
    
    var api = StandardAPI()
    
    var trendingStory = [Story]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menuTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        menuB.addGestureRecognizer(menuTap)
        
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(self.searchFunc(sender:)))
        searchView.addGestureRecognizer(searchTap)
        
        let profileTap = UITapGestureRecognizer(target: self, action: #selector(self.profileFunc(sender:)))
        profile.addGestureRecognizer(profileTap)
        
        let ureportTap = UITapGestureRecognizer(target: self, action: #selector(self.ureportFunc(sender:)))
        uReport.addGestureRecognizer(ureportTap)


        trendingTV.delegate = self
        trendingTV.dataSource = self
        
        api.fetchNewsDelegate = self
        
        
        
        if Connectivity.isConnectedToInternet() {
            
            api.fetchNews(catID: SDConstant.CATEGORY_ID_TRENDING, lastID: SDConstant.LAST_ID)

            
        }else {
            
            let controller = UIAlertController(title: "No Internet Detected", message: "This app requires an Internet connection", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            controller.addAction(ok)
            controller.addAction(cancel)
            
            present(controller, animated: true, completion: nil)
            
            
        }
        
        let rc = UIRefreshControl()
        
        rc.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: UIControlEvents.valueChanged)

        trendingTV.refreshControl = rc

        
        
        // Do any additional setup after loading the view.
    }
    
    func refresh(refreshControl: UIRefreshControl) {

        
        api.fetchNews(catID: SDConstant.CATEGORY_ID_TRENDING, lastID: SDConstant.LAST_ID)
        
        refreshControl.endRefreshing()
    }
    

    func menuFunc(sender:UITapGestureRecognizer) {
        
        Utils.toggleDrawer()
        
    }
    
    func searchFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "search") as! SearchVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func profileFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "profile") as! ProfileVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func ureportFunc(sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "uReport") as! UReportVC
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        trendingStory = news
        
        trendingTV.reloadData()
    }

    
    func didReceiveError(error: String) {
    
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trendingStory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "topTrending", for: indexPath) as! topTrendingCellTableViewCell
            
            cell.topStory = trendingStory[indexPath.row]
            
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "story", for: indexPath) as! trendingCell
            
            cell.topStory = trendingStory[indexPath.row]
            
            return cell
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 210
        }else {
            
            return 150
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"fullStory") as! FullStoryVC
        viewController.story = trendingStory[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
