//
//  opinionCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 11/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class opinionCell: UITableViewCell {

    var topStory : Story? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var videosCount: UILabel!
    
    
    func updateCell() {
        
        
        storyImage.image(fromUrl: (topStory?.img)!)
        
        storyTitle.text = topStory?.title.convertHtml().string
        
        
    }
}
