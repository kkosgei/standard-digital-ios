//
//  SignUpVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UITextFieldDelegate, LoginProtocol {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var signUp: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    
        username.delegate = self
        email.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        
        
        
        let signUpTap = UITapGestureRecognizer(target: self, action: #selector(self.submitRequest))
        signUp.addGestureRecognizer(signUpTap)

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupViews() {
        
        signUp.layer.cornerRadius = 12
        
    }
    
    
    func submitRequest() {
        
        let userName =  username.text!.trim()
        let userEmail = email.text!.trim()
        let passphrase = password.text!.trim()
        let confirmPassphrase = confirmPassword.text!.trim()
        
        if userName.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: username)
            
        } else if userEmail.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: email)
        }
        else if passphrase.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: password)
            
        }
        else if confirmPassphrase.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: confirmPassword)
            
        }
        else if  passphrase != confirmPassphrase {
            
            Utils.shaketextFieldAnimation(myView: confirmPassword)
            
        }
        else {
            
            
            let api = LoginAPI (myDelegate: self)
            
            Utils.showAnimator()
            
            api.signup(username: userName, email: userEmail, password: passphrase)
            
            
            
            
        }
        
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        print("Error Received. Sorry Couldn't Sign You Up")
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        
    }
    
    func didReceiveSuccess(username: String, email: String, name: String, imageUrl: String) {
        
        Utils.hideAnimator()
        
        let user = User(username: username, email: email, name: name, imageUrl: imageUrl)
        
        print("Successfully signed Up \(user.username)")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"drawerController") as! KYDrawerController
        let navController = UINavigationController.init(rootViewController: viewController)
        
        if let window = self.view.window, let rootViewController = window.rootViewController {
            var currentController = rootViewController
            while let presentedController = currentController.presentedViewController {
                currentController = presentedController
            }
            currentController.present(navController, animated: true, completion: nil)
            
        }
        
        Utils.saveUserLoggedIn(user: user)
        
    }

    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        username.resignFirstResponder()
        email.resignFirstResponder()
        password.resignFirstResponder()
        confirmPassword.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(up: true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        animateViewMoving(up: false, moveValue: 100)
    }
    

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
