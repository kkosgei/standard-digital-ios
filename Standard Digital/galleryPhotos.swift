//
//  galleryPhotos.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 13/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class galleryPhotos: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, FetchNewsProtocol, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var galleryCollectionView : UICollectionView!
    
    var api = StandardAPI()
    
    var galleryPhotos = [Story]()
    
    var albumId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        galleryCollectionView.delegate = self
        
        galleryCollectionView.dataSource = self
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        api.fetchNewsDelegate = self
        
        api.fetchGalleryPhotos(catID: SDConstant.CATEGORY_ID_GALLERY, lastID: SDConstant.LAST_ID, albumID: albumId)

        // Do any additional setup after loading the view.
    }

    
    func closeFunc(sender: UIButton) {
        
        if sender.tag == 1 {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        galleryPhotos = news
        
        galleryCollectionView.reloadData()
        
        
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }

        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return galleryPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as! galleryPhotosCollectionViewCell
        
        cell.photo = galleryPhotos[indexPath.row]
        cell.closeB.layer.cornerRadius = 15
        
        cell.closeB.addTarget(self, action: #selector(self.closeFunc), for: .touchUpInside)

    
        return cell
        
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:self.view.frame.size.width, height:self.view.frame.size.height)

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
