//
//  ProfileVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import CoreData

class ProfileVC: UIViewController, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var profileTitle: UILabel!
    
    @IBOutlet weak  var bckBView : UIView!
    
    @IBOutlet weak var bckB : UIButton!
    
    @IBOutlet weak var profileImage : UIImageView!
    
    @IBOutlet weak var savedArticlesCollectionView: UICollectionView!
    
    @IBOutlet weak var profileName : UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var articlesCount: UILabel!
    
    var articles = [SavedStory]()
    
    var picker = UIImagePickerController()
    
    
    @IBOutlet weak var noSavedStoriesView: UIView!
    
    @IBOutlet weak var noSavedStoryLabel : UILabel!
    
    @IBOutlet weak var noSavedStoryImage: UIImageView!
    
    var delStory : SavedStory?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        profileTitle.text = "PROFILE"
        profileImage.isUserInteractionEnabled = true
        
        
        profileName.text = UserDefaults.standard.value(forKey: "User_NameKey") as? String
        
        loadSavedStoriesFromDB()
        
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(self.closeFunc(sender:)))
        bckB.addGestureRecognizer(backTap)
        bckBView.addGestureRecognizer(backTap)
        
        
        let imgTap = UITapGestureRecognizer(target: self, action: #selector(self.pickImage(sender:)))
        profileImage.addGestureRecognizer(imgTap)
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        
        
        if  Utils.retrieveImage() as? UIImage != #imageLiteral(resourceName: "gray") {
            profileImage.image = Utils.retrieveImage() as? UIImage
            profileImage.contentMode = .scaleAspectFill
            
            
        }
        
        savedArticlesCollectionView.delegate = self
        savedArticlesCollectionView.dataSource = self
        
        savedArticlesCollectionView.reloadData()
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width , height : 1200)
        
    }
    
    func pickImage(sender: UITapGestureRecognizer) {
        
        
        changePhotoFunc()
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        profileImage.contentMode = .scaleAspectFill
        
        Utils.storeProfilePic(img: info[UIImagePickerControllerOriginalImage] as? UIImage)
        
        picker.dismiss(animated: true, completion: nil)
        
        Drawer.changeImg()
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func changePhotoFunc() {
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:"Profile Image", message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title: "PICK PHOTO", style:UIAlertActionStyle.default, handler:{ action in self.changeAcc(index: 0)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title: "TAKE PHOTO", style:UIAlertActionStyle.default, handler:{ action in self.changeAcc(index: 1)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        
        settingsActionSheet.popoverPresentationController?.sourceView = profileImage
        settingsActionSheet.popoverPresentationController?.sourceRect = profileImage.bounds
        
        
        present(settingsActionSheet, animated:true, completion:nil)
        
    }
    
    func changeAcc(index:Int){
        
        if index == 0 {
            picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        } else {
            picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = .camera
            present(picker, animated: true, completion: nil)
            
        }
    }
    
    func loadSavedStoriesFromDB() {
        
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate!
        
        let managedContext = appDelegate?.persistentContainer.viewContext
        
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "SavedEntity")
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        
        do {
            
            let newsItems = try managedContext?.fetch(fetchRequest)
            
            for newsItem in newsItems! as [NSManagedObject] {
                
                let newsEntry:SavedStory = SavedStory(id: newsItem.value(forKeyPath: "id") as! Int, category_id: newsItem.value(forKeyPath: "category_id") as! Int, title: newsItem.value(forKeyPath: "title") as! String, author: newsItem.value(forKeyPath: "author") as! String, story: newsItem.value(forKeyPath: "story") as! String, img: newsItem.value(forKeyPath: "img") as! String, link : newsItem.value(forKeyPath: "link") as! String, date: newsItem.value(forKeyPath: "date") as! String, youtube_video_id: newsItem.value(forKeyPath: "youtube_video_id") as! String, keywords: newsItem.value(forKeyPath: "keywords") as! String)
                
                
                self.articles.append(newsEntry)
                
            
            }
            
            DispatchQueue.main.async{
                
                if self.articles.count > 0 {
                    
                    self.noSavedStoriesView.isHidden = true
                    
                }else {
                    
                    self.noSavedStoriesView.isHidden = false
                    
                }
                
                self.articlesCount.text = self.articles.count.description
            }
            
            
        } catch {
            print(error)
        }
        
        
        
    }
    
    
    









func closeFunc(sender: UITapGestureRecognizer) {
    
    self.navigationController?.popViewController(animated: true)
    
}

func numberOfSections(in collectionView: UICollectionView) -> Int {
    
    return 1
    
}



func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return articles.count
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "articleCell", for: indexPath) as! savedArticleCell
    
    if self.articles.count > 0 {
        
        cell.savedStory = self.articles[indexPath.row]
        

        return cell
    }else {
    
    
    return cell
        
    }
}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)

        let viewController = storyBoard.instantiateViewController(withIdentifier: "savedArticle") as! SavedArticleView
        
        viewController.story = self.articles[indexPath.row]
        
        
        
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }






/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

}
