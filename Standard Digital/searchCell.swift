//
//  searchCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 13/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class searchCell: UITableViewCell {

    var topStory : Story? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var storyTime: UILabel!
    
    @IBOutlet weak var shareStory: UIButton!
    
    @IBOutlet weak var saveStory: UIButton!
    
    func updateCell() {
        
        
        storyImage.image(fromUrl: (topStory?.img)!)
        
        storyTitle.text = topStory?.title.convertHtml().string
        
        storyTime.text = Utils.elapsedTime(string: (topStory?.date)!)
        
        
    }

    
    
}
