//
//  Story.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 29/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import SwiftyJSON

class Story {
    
    var _id : Int
    var _category_id : Int
    var _title : String
    var _keywords : String
    var _summary : String
    var _story : String
    var _image_url : String
    var _link : String
    var _date: String
    var _youtube_video_id : String
    
    var id : Int {
        
        return _id
        
    }
    
    var category_id : Int {
        
        return _category_id
        
    }
    
    var title : String {
        
        return _title
        
    }
    
    var keywords : String {
        
        return _keywords
        
    }
    
    var link : String {
        
        return _link
        
    }
    
    var date : String {
        
        return _date
        
    }
    
    var story : String {
        
        return _story
        
    }
    
    var youtube_video_id : String {
        
        return _youtube_video_id
    }
    
    var author : String {
        
        return _summary
    }
    
    var img : String {
        
        return _image_url
    }
    
    
    init(JSONData : [String:JSON]) {
        
        if let id = JSONData["id"]?.intValue{
            
            self._id = id
        }else {
            
            self._id = 0
        }
        
        if let category_id = JSONData["category_id"]?.intValue {
            
            self._category_id = category_id
            
        }
        else {
            
            self._category_id = 0
            
        }
        
        if let title = JSONData["title"]?.stringValue {
            
            self._title = title
            
        }
        else {
            
            self._title = " "
            
        }
        
        if let img = JSONData["image_url"]?.stringValue {
            
            self._image_url = img
            
        }
        else {
            
            self._image_url = " "
            
        }
        
        if let link = JSONData["link"]?.stringValue {
            
            self._link = link
            
        }
        else {
            
            self._link = " "
            
        }
        
        if let date = JSONData["updateddate"]?.stringValue {
            
            self._date = date
            
        }
        else {
            
            self._date = " "
            
        }
        if let author = JSONData["summary"]?.stringValue {
            
            self._summary = author
            
        }
        else {
            
            self._summary = " "
            
        }
        if let youtube_video_id = JSONData["youtube_video_id"]?.stringValue {
            
            self._youtube_video_id = youtube_video_id
            
        }
        else {
            
            self._youtube_video_id = " "
            
        }
        
        if let keywords = JSONData["keywords"]?.stringValue {
            
            self._keywords = keywords
            
        }
        else {
            
            self._keywords = " "
            
        }
        if let story = JSONData["story"]?.stringValue {
            
            self._story = story
            
        }
        else {
            
            self._story = " "
            
        }
 
    }
    
}
