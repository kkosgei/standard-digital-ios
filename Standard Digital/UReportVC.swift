//
//  UReportVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class UReportVC: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
    @IBOutlet weak var ureportImageOne : UIImageView!
    
    @IBOutlet weak var ureportImageTwo: UIImageView!
    
    @IBOutlet weak var ureportImageThree: UIImageView!
    
    @IBOutlet weak var ureportTitle : UITextField!
    
    @IBOutlet weak var ureportLocation: UITextField!
    
    @IBOutlet weak var ureportStory: UITextField!
    
    @IBOutlet weak var submitB : UIButton!
    
    @IBOutlet weak var uReportVCTitle : UILabel!
    
    @IBOutlet weak var bckB: UIButton!
    
    @IBOutlet weak var bckBView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var picker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(self.closeFunc(sender:)))
        
        bckB.addGestureRecognizer(closeTap)
        bckBView.addGestureRecognizer(closeTap)
        
        ureportImageOne.isUserInteractionEnabled = true
        
        ureportImageTwo.isUserInteractionEnabled = true
        
        ureportImageThree.isUserInteractionEnabled = true
        
        
        let imgTap = UITapGestureRecognizer(target: self, action: #selector(self.pickImage(sender:)))
        ureportImageOne.addGestureRecognizer(imgTap)
//        ureportImageTwo.addGestureRecognizer(imgTap)
//        ureportImageThree.addGestureRecognizer(imgTap)
        
        let sendTap = UITapGestureRecognizer(target: self, action: #selector(self.submitRequest))
        submitB.addGestureRecognizer(sendTap)
        
        ureportTitle.delegate = self
        
        ureportLocation.delegate = self
        
        ureportStory.delegate = self
        
        

        
        
        
        // Do any additional setup after loading the view.
    }
    
    func closeFunc(sender: UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func pickImage(sender: UITapGestureRecognizer) {
        
        
        changePhotoFunc()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width , height : 1000)
        
    }
    
    
    
    
    func submitRequest() {
        
        let reportTitle =  self.ureportTitle.text!.trim()
        let location = ureportLocation.text!.trim()
        let story = ureportStory.text!.trim()
        
        if reportTitle.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: ureportTitle)
            
        } else if location.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: ureportLocation)
        }
        else if story.isEmpty {
            
            Utils.shaketextFieldAnimation(myView: ureportStory)
            
        }
        else {
            
            
//           Submit Ureport Story - Upload images
            
            print("Implement The new Stuff")
            
        
            
            
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        ureportTitle.resignFirstResponder()
        ureportLocation.resignFirstResponder()
        ureportStory.resignFirstResponder()
        
        return true
        
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        animateViewMoving(up: true, moveValue: 20)

        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        animateViewMoving(up: false, moveValue: 20)

    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        ureportImageOne.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        
        ureportImageOne.contentMode = .scaleAspectFill
        
        
        
        picker.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func changePhotoFunc() {
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:"Profile Image", message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title: "PICK PHOTO", style:UIAlertActionStyle.default, handler:{ action in self.changeAcc(index: 0)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title: "TAKE PHOTO", style:UIAlertActionStyle.default, handler:{ action in self.changeAcc(index: 1)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        
        settingsActionSheet.popoverPresentationController?.sourceView = ureportImageOne
        settingsActionSheet.popoverPresentationController?.sourceRect = ureportImageOne.bounds

        
        
        present(settingsActionSheet, animated:true, completion:nil)
        
    }
    
    func changeAcc(index:Int){
        
        if index == 0 {
            picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            present(picker, animated: true, completion: nil)
        } else {
            picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = false
            picker.sourceType = .camera
            present(picker, animated: true, completion: nil)
            
        }
    }

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
