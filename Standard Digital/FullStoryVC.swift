//
//  FullStoryVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 23/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import CoreData

class FullStoryVC: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate, FetchNewsProtocol, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var bckB: UIButton!
    
    @IBOutlet weak var bckBView: UIView!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var storyTime: UILabel!
    
    @IBOutlet weak var storyTitle: UILabel!

    @IBOutlet weak var storyAuthor: UILabel!
    
    @IBOutlet weak var storyContent: UITextView!
    
    @IBOutlet weak var relatedTV: UITableView!
    
    @IBOutlet weak var relatedTopics: UICollectionView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var addCommentB : UIButton!
    
    @IBOutlet weak var commentsB: UIButton!
    
    @IBOutlet weak var shareB: UIButton!
    
    @IBOutlet weak var saveB: UIButton!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var bottomBarView: UIView!
    
    
    @IBOutlet weak var relatedView: UIView!
    
    
    @IBOutlet weak var topicsView: UIView!
    
    
    var relatedStory = [Story]()
    
    var story : Story?
    
    var api = StandardAPI()
    
    var relatedStories = [Story]()
    
    var saved : Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        storyContent.adjustUITextViewHeight(arg: self.storyContent)
        
        storyImage.image(fromUrl: (story?.img)!)
        storyTime.text = Utils.elapsedTime(string: (story?.date)!)
        storyTitle.text = story?.title.convertHtml().string
        storyAuthor.text = story?.author.convertHtml().string
        storyContent.text = story?.story.convertHtml().string
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        bckB.addGestureRecognizer(backTap)
        
        let shareTap = UITapGestureRecognizer(target: self, action: #selector(self.shareStory(sender:)))
        shareB.addGestureRecognizer(shareTap)
        
        let commentsTap = UITapGestureRecognizer(target: self, action: #selector(self.loadComments(sender:)))
        addCommentB.addGestureRecognizer(commentsTap)
        commentsB.addGestureRecognizer(commentsTap)
        
        let saveStoryTap = UITapGestureRecognizer(target: self, action: #selector(self.saveStory(sender:)))
        saveB.addGestureRecognizer(saveStoryTap)
        

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        setupTextViewSize()
        
        api.fetchNewsDelegate = self
        
        
//        api.fetchRelated(catID: SDConstant.CATEGORY_ID_RELATED, lastID: SDConstant.LAST_ID, keywords: (story?.keywords)!)
        
        relatedTV.delegate = self
        
        relatedTV.dataSource = self
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.addSubview(relatedTV)
        
        relatedView.isHidden = true
        
        topicsView.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    

   
    func menuFunc(sender:UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func shareStory(sender: UITapGestureRecognizer) {
        
        let activityVC = UIActivityViewController(activityItems: ["\(String(describing: story?.link))"], applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
        
        
    }
    
    
    func saveStory(sender: UITapGestureRecognizer) {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let managedContext = appDelegate?.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "SavedEntity", in: managedContext!)!
        
        if  (saved == false) {
            
            
            saveB.setImage(#imageLiteral(resourceName: "Icons-13"), for: .normal)
            
            saved = true
            
            
            let newsItem = NSManagedObject(entity: entity, insertInto: managedContext)
            
            newsItem.setValue(story?.id, forKey: "id")
            newsItem.setValue(story?.category_id, forKey: "category_id")
            newsItem.setValue(story?.title, forKey: "title")
            newsItem.setValue(story?.img, forKey: "img")
            newsItem.setValue(story?.link, forKey: "link")
            newsItem.setValue(story?.date, forKey: "date")
            newsItem.setValue(story?.author, forKey: "author")
            newsItem.setValue(story?.youtube_video_id, forKey: "youtube_video_id")
            newsItem.setValue(story?.story, forKey: "story")
            newsItem.setValue(story?.keywords, forKey: "keywords")
            
            
            do {
                
                try managedContext?.save()
                
            }catch let error as NSError {
                
                print("Could not save. \(error), \(error.userInfo)")
            }
        }else {
            
            
            saveB.setImage(#imageLiteral(resourceName: "unfavorite"), for: .normal)
            saved = false
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"SavedEntity")
            
            fetchRequest.predicate = NSPredicate(format: "id = %@", "\(story?.id)")
            
            do {
                
                let fetchedResults =  try managedContext!.fetch(fetchRequest) as? [NSManagedObject]
                
                for entity in fetchedResults! {
                    
                    
                    managedContext?.delete(entity)
                }
                
            }catch {
                
                print("Could not delete")
                
            }
            
            
        }
        
        
    }
    
    
    func loadComments(sender: UITapGestureRecognizer) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let viewController = storyBoard.instantiateViewController(withIdentifier: "comments") as! CommentsVC
        
//        viewController.story = self.story
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
    func setupTextViewSize() {
        
        let fixedWidth = storyContent.frame.size.width
        
        storyContent.sizeThatFits(CGSize(fixedWidth,CGFloat.greatestFiniteMagnitude))
        
        let newSize = storyContent.sizeThatFits(CGSize(fixedWidth,CGFloat.greatestFiniteMagnitude))
        
        var newFrame = storyContent.frame
        
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        storyContent.frame = newFrame
       
        storyContent.isScrollEnabled = false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
        
    }
    
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        relatedStories = news
        
        relatedTV.reloadData()
        
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        

        
    }
    
    
    
    

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return relatedStories.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "related", for: indexPath) as! relatedCell
        
        cell.relatedStory = relatedStories[indexPath.row]
        
        return cell
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
