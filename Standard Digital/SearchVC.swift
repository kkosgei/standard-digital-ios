//
//  SearchVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 28/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource , FetchNewsProtocol, UITextFieldDelegate{
    
    @IBOutlet weak var searchTV : UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var closeBView: UIView!
    
    @IBOutlet weak var closeB: UIButton!
    
    
    var searchResults = [Story]()
    
    var api = StandardAPI()
    
    var searchString : String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        searchTV.delegate = self
        
        searchTV.dataSource = self
        
        api.fetchNewsDelegate = self
        
        searchTextField.delegate = self
        
        let closeTap = UITapGestureRecognizer(target: self, action: #selector(self.closeView(sender:)))
        closeB.addGestureRecognizer(closeTap)
        
        let closeViewTap = UITapGestureRecognizer(target: self, action: #selector(self.closeView(sender:)))
        closeBView.addGestureRecognizer(closeViewTap)

        
        
        // Do any additional setup after loading the view.
    }
    
    func closeView(sender: UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    func didReceiveSuccess(news: [Story]) {
        
        Utils.hideAnimator()
        
        searchResults = news
        
        searchTV.reloadData()
        
        
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
        
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return searchResults.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! searchCell
        
        cell.topStory = searchResults[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "fullStory") as! FullStoryVC
        viewController.story = searchResults[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        searchTextField.resignFirstResponder()
        
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        searchString = searchTextField.text!
        
        api.fetchSearch(catID: SDConstant.CATEGORY_ID_SEARCH, lastID: SDConstant.LAST_ID, searchCritea: searchString)

        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
