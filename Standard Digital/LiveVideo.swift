//
//  LiveVideo.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 02/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import Foundation
import SwiftyJSON

class LiveKtn {
    

    var _youtube_video_id : String
    
    
    var youtube_video_id : String {
        
        return _youtube_video_id
    }
    
    
    init(JSONData : [String:JSON]) {
        
               if let youtube_video_id = JSONData["videoId"]?.stringValue {
            
            self._youtube_video_id = youtube_video_id
            
        }
        else {
            
            self._youtube_video_id = " "
            
        }
        
        
    }
    
}
