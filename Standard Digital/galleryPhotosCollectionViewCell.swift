//
//  galleryPhotosCollectionViewCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 13/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class galleryPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    
    @IBOutlet weak var photoTitle: UILabel!
    
    
    @IBOutlet weak var closeB: UIButton!
    
    
    var photo : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        photoImage.image(fromUrl: (photo?.img)!)
        photoTitle.text = photo?.title.convertHtml().string
        
    }
    
}
