//
//  savedArticleCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 15/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class savedArticleCell: UICollectionViewCell {
    
    @IBOutlet weak var articleImage: UIImageView!
    
    @IBOutlet weak var articleTitle: UILabel!
    
    @IBOutlet weak var articlePostedTime: UILabel!
    
    @IBOutlet weak var deleteArticle: UIButton!
    
    
    var savedStory : SavedStory? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    func updateCell() {
        
        
        articleImage.image(fromUrl: (savedStory?.img)!)
        
        articleTitle.text = savedStory?.title.convertHtml().string
        
        articlePostedTime.text = Utils.elapsedTime(string: (savedStory?.date)!)
        
        deleteArticle.isHidden = true
        
        
    }
    
    
}
