//
//  relatedCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 18/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class relatedCell: UITableViewCell {

   
    var relatedStory : Story? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var storyTime: UILabel!
    
    @IBOutlet weak var shareStory: UIButton!
    
    @IBOutlet weak var saveStory: UIButton!
    
    func updateCell() {
        
        
        storyImage.image(fromUrl: (relatedStory?.img)!)
        
        storyTitle.text = relatedStory?.title.convertHtml().string
        
        storyTime.text = Utils.elapsedTime(string: (relatedStory?.date)!)
        
        
    }

    
}
