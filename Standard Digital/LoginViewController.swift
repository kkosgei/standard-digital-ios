//
//  LoginViewController.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    
    @IBOutlet weak var loginContainer: UIView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    private lazy var signIn: SignInVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "signIn") as! SignInVC
        
        // Add View Controller as Child View Controller
        
        self.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: self.loginContainer.frame.width, height: self.loginContainer.frame.height)
        self.loginContainer.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        return viewController
    }()
    
    private lazy var signUp: SignUpVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "signUp") as! SignUpVC
        
        // Add View Controller as Child View Controller
        self.addChildViewController(viewController)
        
        
        return viewController
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        setupView()
        
        
    }
    
    
    private func setupView() {
        setupSegmentedControl()
        
        updateView()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: signUp)
            add(asChildViewController: signIn)
        } else {
            remove(asChildViewController: signIn)
            add(asChildViewController: signUp)
        }
    }
    
    private func setupSegmentedControl() {
        // Configure Segmented Control
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "SIGN IN", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "SIGN UP", at: 1, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        
        // Select First Segment
        segmentedControl.selectedSegmentIndex = 0
    }
    
    
    func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        //        viewController.view.frame = view.bounds
        //        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        viewController.view.frame = CGRect(x: 0, y: 0, width: self.loginContainer.frame.width, height: self.loginContainer.frame.height)
        self.loginContainer.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
        
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
