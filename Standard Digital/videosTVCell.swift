//
//  videosTVCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 12/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class videosTVCell: UITableViewCell {

    @IBOutlet weak var videoImage : UIImageView!
    
    @IBOutlet weak var videoTitle : UILabel!
    
    @IBOutlet weak var postedTime : UILabel!
    
    @IBOutlet weak var saveB : UIButton!
    
    @IBOutlet weak var shareB : UIButton!
    
    
    
    var video : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        let videoId = video?.youtube_video_id.replacingOccurrences(of: "\"", with: "")
        
        let imageUrl = "https://img.youtube.com/vi/"+videoId!+"/sddefault.jpg"
        videoImage.image(fromUrl: imageUrl)
        videoTitle.text = video?.title.convertHtml().string
        postedTime.text = Utils.elapsedTime(string: (video?.date)!)
        
        
    }

    
    
    
    
}
