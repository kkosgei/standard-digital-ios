//
//  SignInVC.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 14/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class SignInVC: UIViewController, UITextFieldDelegate, LoginProtocol {
    
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        
        email.delegate = self
        
        password.delegate = self
        
        
        let menuTap = UITapGestureRecognizer(target: self, action: #selector(self.submitRequest))
        signInButton.addGestureRecognizer(menuTap)
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        
        
        signInButton.layer.cornerRadius = 12
        
    }
    
    func submitRequest() {
        
        print("::::: Submit Sign In Request:::::")
        
        let username = self.email.text?.trim()
        let pass = self.password.text?.trim()
        
        if (username?.isEmpty)! {
            
            Utils.shaketextFieldAnimation(myView: email)
        }else if (pass?.isEmpty)! {
            
            Utils.shaketextFieldAnimation(myView: password)
            
        }else {
            
            
//    Encryption to be done here.
            
            let api = LoginAPI(myDelegate: self)
            
            Utils.showAnimator()
            
            api.login(username: username!, password: pass!)
        }
        
        
        
        
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    
    func didReceiveSuccess(username: String, email: String, name: String, imageUrl: String) {
        
        Utils.hideAnimator()
        
        print("Logged in with Success ------------->>")
        
        let user = User(username: username, email: email, name: name, imageUrl: imageUrl)
        
        print("Welcome \(user.email)")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier :"drawerController") as! KYDrawerController
        let navController = UINavigationController.init(rootViewController: viewController)
        
        if let window = self.view.window, let rootViewController = window.rootViewController {
            var currentController = rootViewController
            while let presentedController = currentController.presentedViewController {
                currentController = presentedController
            }
            currentController.present(navController, animated: true, completion: nil)
            
        }
        
        Utils.saveUserLoggedIn(user: user)
        
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
   
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        email.resignFirstResponder()
        password.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(up: true, moveValue: 50)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        animateViewMoving(up: false, moveValue: 50)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
