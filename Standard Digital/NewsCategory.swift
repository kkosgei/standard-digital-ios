//
//  NewsCategory.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 13/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class NewsCategory: UIViewController, UITableViewDelegate, UITableViewDataSource, FetchNewsProtocol, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var backBView: UIView!
    
    @IBOutlet weak var bckB: UIButton!
    
    @IBOutlet weak var newsCategoryTV : UITableView!
    
    @IBOutlet weak var categoryTitle: UILabel!
    
    var catTitle : String = ""
    
    var catID : Int = 0
    
    var api = StandardAPI()
    
    var categoryNews = [Story]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsCategoryTV.delegate = self
        
        newsCategoryTV.dataSource = self
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        api.fetchNewsDelegate = self
        
        if Connectivity.isConnectedToInternet() {
            
            api.fetchNews(catID: catID, lastID: SDConstant.LAST_ID)
            
            
        }else {
            
            let controller = UIAlertController(title: "No Internet Detected", message: "This app requires an Internet connection", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            controller.addAction(ok)
            controller.addAction(cancel)
            
            present(controller, animated: true, completion: nil)
            
            
        }
        
        
        categoryTitle.text = catTitle
        
        
        let bckViewTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        backBView.addGestureRecognizer(bckViewTap)
        
        let bckBTap = UITapGestureRecognizer(target: self, action: #selector(self.menuFunc(sender:)))
        bckB.addGestureRecognizer(bckBTap)


        let rc = UIRefreshControl()
        
        rc.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: UIControlEvents.valueChanged)

        newsCategoryTV.refreshControl = rc
    }
    
    func menuFunc(sender:UITapGestureRecognizer) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func refresh(refreshControl: UIRefreshControl) {
        
        api.fetchNews(catID: catID, lastID: SDConstant.LAST_ID)

        
        refreshControl.endRefreshing()
        
    }

  
    func didReceiveSuccess(news: [Story]) {
        
        
        Utils.hideAnimator()
        
        categoryNews = news
        
        newsCategoryTV.reloadData()
    }
    
    func didReceiveError(error: String) {
        
        Utils.hideAnimator()
        
        DispatchQueue.main.async {
            
            let  alert = UIAlertController(title: "Failure", message: error, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            
            self.present(alert,animated: true,completion: nil)
        }

        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return categoryNews.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "topNews", for: indexPath)
        as! topNewsCell
        
        cell.topStory = categoryNews[indexPath.row]
        return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "news", for: indexPath) as!
            
            newsCell
            
            cell.topStory = categoryNews[indexPath.row]
            
            return cell
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 210
        }else {
            
            return 150
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController =   storyboard.instantiateViewController(withIdentifier: "fullStory") as! FullStoryVC
        viewController.story = categoryNews[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
