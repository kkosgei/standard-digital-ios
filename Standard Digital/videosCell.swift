//
//  videosCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 01/12/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit

class videosCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var videoImage: UIImageView!
        
    @IBOutlet weak var videoTitle: UILabel!
    
    
    
    var video : Story? {
        
        didSet {
            
            updateCell()
            
        }
        
    }
    
    
    
    func updateCell() {
        
        let videoId = video?.youtube_video_id.replacingOccurrences(of: "\"", with: "")
        
        let imageUrl = "https://img.youtube.com/vi/"+videoId!+"/sddefault.jpg"
        videoImage.image(fromUrl: imageUrl)
        videoTitle.text = video?.title.convertHtml().string
     
    }
}
