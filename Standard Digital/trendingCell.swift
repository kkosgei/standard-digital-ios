//
//  trendingCell.swift
//  Standard Digital
//
//  Created by Kelvin Kosgei on 30/11/2017.
//  Copyright © 2017 Kocela Limited. All rights reserved.
//

import UIKit
import CoreData

class trendingCell: UITableViewCell {

    var topStory : Story? {
        
        didSet {
            
            updateCell()
        }
        
        
    }
    
    @IBOutlet weak var storyTitle: UILabel!
    
    @IBOutlet weak var storyImage: UIImageView!
    
    @IBOutlet weak var storyTime: UILabel!
    
    @IBOutlet weak var shareStory: UIButton!
    
    @IBOutlet weak var saveStory: UIButton!
    
    var saved : Bool = false
    
    func updateCell() {
        
        
        storyImage.image(fromUrl: (topStory?.img)!)
        
        storyTitle.text = topStory?.title.convertHtml().string
        
        storyTime.text = Utils.elapsedTime(string: ((topStory?.date))!)
        
        let saveStoryTap = UITapGestureRecognizer(target: self, action: #selector(self.saveStory(sender:)))
        saveStory.addGestureRecognizer(saveStoryTap)

    }
    
    
    func saveStory(sender: UITapGestureRecognizer) {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let managedContext = appDelegate?.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "SavedEntity", in: managedContext!)!
        
        if  (saved == false) {
            
            
            saveStory.setImage(#imageLiteral(resourceName: "Icons-13"), for: .normal)
            
            saved = true
            
            
            let newsItem = NSManagedObject(entity: entity, insertInto: managedContext)
            
            newsItem.setValue(topStory?.id, forKey: "id")
            newsItem.setValue(topStory?.category_id, forKey: "category_id")
            newsItem.setValue(topStory?.title, forKey: "title")
            newsItem.setValue(topStory?.img, forKey: "img")
            newsItem.setValue(topStory?.link, forKey: "link")
            newsItem.setValue(topStory?.date, forKey: "date")
            newsItem.setValue(topStory?.author, forKey: "author")
            newsItem.setValue(topStory?.youtube_video_id, forKey: "youtube_video_id")
            newsItem.setValue(topStory?.story, forKey: "story")
            newsItem.setValue(topStory?.keywords, forKey: "keywords")
            
            
            do {
                
                try managedContext?.save()
                
            }catch let error as NSError {
                
                print("Could not save. \(error), \(error.userInfo)")
            }
        }else {
            
            
            saveStory.setImage(#imageLiteral(resourceName: "unfavorite"), for: .normal)
            saved = false
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"SavedEntity")
            
            fetchRequest.predicate = NSPredicate(format: "date = %@", "\(topStory?.date)")
            
            do {
                
                let fetchedResults =  try managedContext!.fetch(fetchRequest) as? [SavedEntity]
                
                for entity in fetchedResults! {
                    
                    
                    managedContext?.delete(entity)
                }
                
            }catch {
                
                print("Could not delete")
                
            }
            
            
        }
        
        
    }




}
